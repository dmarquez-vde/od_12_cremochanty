# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010-Today Vos Datos Enterprise Suite SA de CV
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'VDE Cremo Chanty',
    'version': '1.0',
    'depends': [
            'sale_management', 'base', 'mrp'
    ],
    'license': 'Other proprietary',
    'price': 999.0,
    'category' : 'Sales',
    'currency': 'EUR',
    'summary': """Custom Module for Cremo Chanty""",
    'description': "",
    'author': 'VDE Suite',
    'website': 'https://www.vde-suite.com',
    'support': '911@vde-suite.com.com',
    'images': [],
#    'live_test_url': 'https://vde-suite.com',
    'data':[
        #'security/ir.model.access.csv',
    ],
    'installable' : True,
    'application' : False,
}
